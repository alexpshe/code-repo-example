package com.code.example;

import org.testng.annotations.Test;

public class ServiceTest {
    @Test
    public void serviceTest() {
        Service service = new Service();
        assert service.execute() == 4;
    }
}
